export const CHIP_ICONS = {
  city: "mdi-map-marker-outline",
  start_at: "mdi-calendar-minus",
  place: "mdi-city-variant",
  min_age: "mdi-account-outline",
  is_offline: "mdi-guy-fawkes-mask",
  tags: "mdi-menu",
};
