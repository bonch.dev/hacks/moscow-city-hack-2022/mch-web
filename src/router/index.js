import Vue from "vue";
import VueRouter from "vue-router";
import Home from "../views/Home.vue";
import DefaultLayout from "@/components/layouts/DefaultLayout.vue";
import AuthLayout from "@/components/layouts/AuthLayout.vue";

Vue.use(VueRouter);

const routes = [
  {
    path: "/",
    component: DefaultLayout,
    children: [
      {
        path: "",
        name: "Home",
        component: Home,
      },
      {
        path: "events",
        name: "Events",
        component: () => import("@/views/Events.vue"),
      },
      {
        path: "volunteers",
        name: "Volunteers",
        component: () => import("@/views/Volunteers.vue"),
      },
      {
        path: "integration",
        name: "Integration",
        component: () => import("@/views/Integration.vue"),
      },
      {
        path: "qr/:id",
        name: "QR",
        component: () => import("@/views/QR.vue"),
      },
      {
        path: "chat/:id",
        name: "Chat",
        component: () => import("@/views/Chat.vue"),
      },
    ],
  },
  {
    path: "/auth",
    component: AuthLayout,
    children: [
      {
        path: "",
        name: "Auth",
        component: () => import("@/views/Auth.vue"),
      },
    ],
  },
];

const router = new VueRouter({
  mode: "history",
  base: process.env.BASE_URL,
  routes,
});

router.beforeEach((to, from, next) => {
  if (!localStorage.getItem("user-token") && to.path !== "/auth") {
    next("/auth");
  } else {
    next();
  }
});

export default router;
