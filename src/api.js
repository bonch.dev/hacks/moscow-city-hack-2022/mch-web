import axios from "axios";

const baseURL = "https://mch-backend-staging.server.bonch.dev/";

const API = axios.create({
  baseURL: baseURL,
  withCredentials: true,
});

API.interceptors.request.use(
  (config) => {
    // Do something before request is sent
    const token = localStorage.getItem("user-token");
    if (token) {
      config.headers.common["Authorization"] = "Bearer " + token;
    }
    return config;
  },
  (error) => {
    // Do something with request error
    return Promise.reject(error);
  }
);

API.interceptors.response.use(
  (response) => {
    // Do something with response data
    return response;
  },
  (error) => {
    if (error.response.status === 401) {
      localStorage.removeItem("user-token");
      location.href = "/auth";
    }
    // Do something with response error
    return Promise.reject(error);
  }
);

export default API;
