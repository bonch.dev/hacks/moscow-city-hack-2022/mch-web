import API from "@/api.js";

const state = {};

const getters = {};

const actions = {
  sendNotification(_, payload) {
    return new Promise((resolve, reject) => {
      API.post("api/panel/notifications", {
        message: payload.message,
      })
        .then((response) => {
          resolve(response);
        })
        .catch((error) => {
          reject(error);
        });
    });
  },
};

const mutations = {};

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations,
};
