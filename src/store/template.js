import API from "@/api.js";

const state = {
  template: null,
};

const getters = {};

const actions = {
  loadTemplate({ commit }) {
    return new Promise((resolve, reject) => {
      API.get("api/panel/templates")
        .then((response) => {
          commit("SET_TEMPLATE", response.data.data);
        })
        .then((response) => resolve(response))
        .catch((error) => reject(error));
    });
  },

  updateTemplate(_, payload) {
    return new Promise((resolve, reject) => {
      API.post("api/panel/templates", {
        organization_title: payload.organization,
        general_title: payload.fullName,
      })
        .then((response) => resolve(response))
        .catch((error) => reject(error));
    });
  },

  storeSeal(_, payload) {
    let formData = new FormData();
    formData.append("file", payload.image, payload.image.name);

    return new Promise((resolve, reject) => {
      API.post("api/panel/templates/seal", formData)
        .then((response) => resolve(response))
        .catch((error) => reject(error));
    });
  },

  storeSign(_, payload) {
    let formData = new FormData();
    formData.append("file", payload.image, payload.image.name);

    return new Promise((resolve, reject) => {
      API.post("api/panel/templates/sign", formData)
        .then((response) => resolve(response))
        .catch((error) => reject(error));
    });
  },
};

const mutations = {
  SET_TEMPLATE(state, payload) {
    state.template = payload;
  },
};

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations,
};
