import API from "@/api.js";

const state = {};

const getters = {};

const actions = {
  auth(_, payload) {
    return new Promise((resolve, reject) => {
      API.post("api/panel/auth/login", {
        login: payload.login,
        password: payload.password,
      })
        .then((response) => {
          localStorage.setItem(
            "broadcast-token",
            response.data.data.broadcast_token
          );
          localStorage.setItem("user-token", response.data.data.access_token);

          API.defaults.headers.common["Authorization"] =
            "Bearer " + response.data.access_token;

          resolve(response);
        })
        .catch((error) => {
          reject(error);
        });
    });
  },
};

const mutations = {};

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations,
};
