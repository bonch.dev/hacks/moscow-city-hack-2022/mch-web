import API from "@/api.js";

const state = {
  members: [],
  participants: [],
  users: [],
};

const getters = {};

const actions = {
  loadMembers({ commit }) {
    return new Promise((resolve, reject) => {
      API.get("api/panel/users/members")
        .then((response) => {
          commit("SET_MEMBERS", response.data.data);

          resolve(response);
        })
        .catch((error) => {
          reject(error);
        });
    });
  },

  loadParticipants({ commit }) {
    return new Promise((resolve, reject) => {
      API.get("api/panel/users/participants")
        .then((response) => {
          commit("SET_PARTICIPANTS", response.data.data);

          resolve(response);
        })
        .catch((error) => {
          reject(error);
        });
    });
  },

  searchUsers({ commit }, payload) {
    return new Promise((resolve, reject) => {
      API.get("api/panel/users/search", {
        params: {
          search: payload.search,
        },
      })
        .then((response) => {
          commit("SET_USERS", response.data.data);

          resolve(response);
        })
        .catch((error) => {
          reject(error);
        });
    });
  },

  inviteUser(_, payload) {
    return new Promise((resolve, reject) => {
      API.post(`api/panel/users/${payload.userId}/invite`)
        .then((response) => {
          resolve(response);
        })
        .catch((error) => {
          reject(error);
        });
    });
  },
};

const mutations = {
  SET_MEMBERS(state, payload) {
    state.members = payload;
  },

  SET_PARTICIPANTS(state, payload) {
    state.participants = payload;
  },

  SET_USERS(state, payload) {
    state.users = payload;
  },
};

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations,
};
