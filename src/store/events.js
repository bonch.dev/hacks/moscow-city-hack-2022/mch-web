import API from "@/api.js";

const state = {
  events: [],
};

const getters = {};

const actions = {
  loadEvents({ commit }) {
    return new Promise((resolve, reject) => {
      API.get("api/panel/events")
        .then((response) => {
          commit("SET_EVENTS", response.data.data);

          resolve(response);
        })
        .catch((error) => {
          reject(error);
        });
    });
  },

  loadEvent({ commit }, payload) {
    return new Promise((resolve, reject) => {
      API.get(`api/panel/events/${payload.eventId}`)
        .then((response) => {
          commit("SET_EVENT", response.data.data);

          resolve(response);
        })
        .catch((error) => {
          reject(error);
        });
    });
  },

  createEvent(_, payload) {
    return new Promise((resolve, reject) => {
      API.post("api/panel/events", {
        title: payload.title,
        description: payload.description,
        place: payload.place,
        city_id: payload.city_id,
        start_at: `${payload.date_start_at}T${payload.time_start_at}:00Z`,
        min_age: payload.min_age,
        is_offline: payload.is_offline,
        tags: payload.tags.map((tag) => {
          return tag.id;
        }),
        roles: payload.roles,
      })
        .then((response) => {
          resolve(response);
        })
        .catch((error) => {
          reject(error);
        });
    });
  },

  updateEvent(_, payload) {
    return new Promise((resolve, reject) => {
      API.patch(`api/panel/events/${payload.eventId}`, {
        title: payload.title,
        description: payload.description,
        place: payload.place,
        city_id: payload.city_id,
        start_at: `${payload.date_start_at}T${payload.time_start_at}:00Z`,
        min_age: payload.min_age,
        is_offline: payload.is_offline,
        tags: payload.tags.map((tag) => {
          return tag.id;
        }),
        roles: payload.roles,
      })
        .then((response) => {
          resolve(response);
        })
        .catch((error) => {
          reject(error);
        });
    });
  },

  uploadImage(_, payload) {
    let formData = new FormData();
    formData.append("file", payload.image, payload.image.name);

    return new Promise((resolve, reject) => {
      API.post(`api/panel/events/${payload.eventId}/media`, formData)
        .then((response) => {
          resolve(response);
        })
        .catch((error) => {
          reject(error);
        });
    });
  },

  deleteEvent(_, payload) {
    return new Promise((resolve, reject) => {
      API.delete(`api/panel/events/${payload.eventId}`)
        .then((response) => {
          resolve(response);
        })
        .catch((error) => {
          reject(error);
        });
    });
  },

  activateEvent(_, payload) {
    return new Promise((resolve, reject) => {
      API.post(`api/panel/events/${payload.eventId}/activate`)
        .then((response) => {
          resolve(response);
        })
        .catch((error) => {
          reject(error);
        });
    });
  },

  deactivateEvent(_, payload) {
    return new Promise((resolve, reject) => {
      API.post(`api/panel/events/${payload.eventId}/deactivate`)
        .then((response) => {
          resolve(response);
        })
        .catch((error) => {
          reject(error);
        });
    });
  },

  approveQRCode(_, payload) {
    return new Promise((resolve, reject) => {
      API.post(`api/panel/events/${payload.eventId}/approve`, {
        code: payload.code,
      })
        .then((response) => {
          resolve(response);
        })
        .catch((error) => {
          reject(error);
        });
    });
  },
};

const mutations = {
  SET_EVENTS(state, payload) {
    state.events = payload;
  },

  SET_EVENT(state, payload) {
    state.event = payload;
  },
};

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations,
};
