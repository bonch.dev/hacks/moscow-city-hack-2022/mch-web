import API from "@/api.js";

const state = {
  user: null,
};

const getters = {};

const actions = {
  checkUser({ commit }) {
    return new Promise((resolve, reject) => {
      API.get("api/panel/me")
        .then((response) => {
          commit("SET_USER", response.data.data);

          resolve(response);
        })
        .catch((error) => {
          reject(error);
        });
    });
  },

  updateUser(_, payload) {
    return new Promise((resolve, reject) => {
      API.patch("api/panel/me", {
        name: payload.name,
        description: payload.description,
        contacts: payload.contacts,
      })
        .then((response) => {
          resolve(response);
        })
        .catch((error) => {
          reject(error);
        });
    });
  },

  uploadImage(_, payload) {
    let formData = new FormData();
    formData.append("file", payload.image, payload.image.name);

    return new Promise((resolve, reject) => {
      API.post(`api/panel/me/media`, formData)
        .then((response) => {
          resolve(response);
        })
        .catch((error) => {
          reject(error);
        });
    });
  },
};

const mutations = {
  SET_USER(state, payload) {
    state.user = payload;
  },
};

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations,
};
