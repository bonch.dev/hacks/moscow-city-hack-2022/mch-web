import API from "@/api.js";

const state = {
  chat: null,
};

const getters = {};

const actions = {
  sendMessage(_, payload) {
    return new Promise((resolve, reject) => {
      API.post(`api/panel/chats/${payload.chatId}/message`, {
        message: payload.message,
      })
        .then((response) => {
          resolve(response);
        })
        .catch((error) => {
          reject(error);
        });
    });
  },

  loadChat({ commit }, payload) {
    return new Promise((resolve, reject) => {
      API.get(`api/panel/chats/${payload.chatId}`)
        .then((response) => {
          commit("SET_CHAT", response.data.data);

          resolve(response);
        })
        .catch((error) => {
          reject(error);
        });
    });
  },
};

const mutations = {
  SET_CHAT(state, payload) {
    state.chat = payload;
  },
};

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations,
};
