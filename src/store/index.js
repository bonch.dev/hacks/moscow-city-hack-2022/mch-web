import Vue from "vue";
import Vuex from "vuex";
import auth from "./auth.js";
import user from "./user.js";
import events from "./events.js";
import volunteers from "./volunteers.js";
import chat from "./chat.js";
import notifications from "./notifications.js";
import template from "./template.js";

Vue.use(Vuex);

export default new Vuex.Store({
  state: {},

  mutations: {},

  actions: {},

  modules: {
    auth,
    user,
    events,
    volunteers,
    chat,
    notifications,
    template,
  },
});
